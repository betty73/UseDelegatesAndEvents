﻿using System;
using System.Collections;

namespace System.Collections.Generic
{
    public static class CollectionExtensions
    {
        public static T GetMax<T>(this IEnumerable e ,Func<T,float> getParameter) where T : class 
        {
            var maxValue = float.MinValue;
            var result = default(T);
            foreach (T item in e)
            {
                var value = getParameter(item);
                if (value > maxValue)
                {
                    maxValue = value;
                    result = item;
                }    
            }
            return result;
        }
    }
}
