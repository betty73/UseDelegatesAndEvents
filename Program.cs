﻿// See https://aka.ms/new-console-template for more information
using System.Collections;
using UseDelegatesAndEvents;
Console.WriteLine("Test CollectionExtention:");

Func<string, float> getParameter = (obj) =>
{
    Console.WriteLine($"Element:{obj} Length:{obj.Length}");
    return (float)obj.Length;
};
var collection = new List<string>() {"Добр бобер, да не добра лиса.", "Добро не в деньгах, а в людях.", "Нет худа без добра.", "Не делай добра, не получишь и зла"};
var maxElement =CollectionExtensions.GetMax(collection, getParameter);
Console.WriteLine($"Max element:{maxElement} Length:{maxElement.Length}");

Console.WriteLine("Test FileInspector, stopping with contains '.cpp':");

var searcher = new FileInspector();
searcher.FileFound += OnFileFound;
searcher.SearchFiles(@"D:\Проекты\", @"*.*");
searcher.FileFound -= OnFileFound;

void OnFileFound(object? sender, FileArgs args)
{
    string? name = args.FileName ?? String.Empty;
    Console.WriteLine($"FoundFile:{name}");
    args.IsCancel = name.Contains(".cpp");
}

