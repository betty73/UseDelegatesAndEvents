﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseDelegatesAndEvents
{
    internal class FileInspector
    {
        public delegate void EventHandler(object sender, FileArgs args);
        public event EventHandler? FileFound;
        public void SearchFiles(string path,string search)
        {
            foreach(var fileName in Directory.EnumerateFiles(path, search, SearchOption.AllDirectories))
            {
                var args = new FileArgs() { FileName = fileName };
                FileFound?.Invoke(this, args);
                if (args.IsCancel)
                    break;
            }
        }
    }
}
