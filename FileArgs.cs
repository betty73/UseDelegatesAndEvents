﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseDelegatesAndEvents
{
    internal class FileArgs :EventArgs
    {
        public string? FileName { get; set; }
        public bool IsCancel{ get; set; }
    }
}
